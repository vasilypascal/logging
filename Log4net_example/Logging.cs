﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using log4net.Config; 

namespace Log4net_example
{
    public static class Logging
    {
        private static ILog _logger = LogManager.GetLogger("LOGGER");

        public static ILog Log
        {
            get { return _logger; }
        }
        public static void InitLogger()
        {
            XmlConfigurator.Configure();
        }
    }
}
