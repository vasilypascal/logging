﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;


namespace Log4net_example
{
    public class ConfigManager : IConfigManager
    {
        public bool GetBoolValue(string key)
        {
           return ConfigurationManager.AppSettings[key] == "true";
        }

        public int? GetIntNullableValue(string key)
        {
            int result;
            if (int.TryParse(ConfigurationManager.AppSettings[key], out result)) return result;
            return null;
        }

        public string GetValue(string key)
        {
            return ConfigurationManager.AppSettings[key];
        }
    }

}
