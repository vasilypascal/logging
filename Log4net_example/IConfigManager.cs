﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using System.Configuration;

namespace Log4net_example
{
    public interface IConfigManager
    {
        int? GetIntNullableValue(string Key);
        bool GetBoolValue(string Key);
        string GetValue(string Key);
    }
}
