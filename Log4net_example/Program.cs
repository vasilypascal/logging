﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using log4net.Config;
using System.Configuration;
using Log4net_example;

namespace Log4net_example
{
    class Program
    {
        static void Main(string[] args)
        {
            //Logging.InitLogger();
            //Logging.Log.Info("My first Log :)");

            ConfigManager config = new ConfigManager();

            Console.WriteLine("\n" + config.GetIntNullableValue("example1"));

            Console.WriteLine("\n" + config.GetBoolValue("example2"));

            Console.WriteLine("\n" + config.GetValue("example3"));




            Console.ReadKey();
        }
    }
}
